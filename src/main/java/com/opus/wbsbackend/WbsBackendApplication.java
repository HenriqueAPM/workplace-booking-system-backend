package com.opus.wbsbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WbsBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(WbsBackendApplication.class, args);
    }

}
